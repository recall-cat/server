<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function a_user_can_be_create_without_information()
    {
        $user = User::create();
        $this->assertDatabaseHas('users', [
            'id' => $user->id
        ]);
    }

    /** @test */
    public function a_local_user_can_provide_a_passport_token()
    {
        $this->passportInstall();
        $user = User::create();
        $token = $user->createToken('Token Name')->accessToken;
        $this->assertNotEmpty($token);
    }
}
