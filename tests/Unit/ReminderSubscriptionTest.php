<?php

namespace Tests\Unit;

use App\Event;
use App\Subscription;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReminderSubscriptionTest extends TestCase
{
    /** @test */
    public function a_reminder_can_have_many_subscriptions()
    {
        $reminder = create('App\Reminder');
        $create = 3;
        factory(Subscription::class, $create)->create([
            'reminder_id' => $reminder->id
        ]);
        $count = $reminder->fresh()->subscriptions->count();
        $this->assertEquals($count, $create);
    }

    /** @test */
    public function a_reminder_can_aggregate_subscription_by_user()
    {
        $reminder = create('App\Reminder');
        $userA = create('App\User');
        $userB = create('App\User');
        factory(Subscription::class)->states('browser', 'trigger')->create([
            'user_id' => $userA->id,
            'reminder_id' => $reminder->id
        ]);
        factory(Subscription::class)->states('mail', 'trigger')->create([
            'user_id' => $userB->id,
            'reminder_id' => $reminder->id
        ]);
        factory(Subscription::class)->states('browser', 'trigger')->create([
            'user_id' => $userB->id,
            'reminder_id' => $reminder->id
        ]);
        $count = $reminder->fresh()->subscriptions->count();
        $this->assertEquals($count, 3);
        $subscribers = $reminder->subscribers(Event::fromName('trigger'));
        $this->assertEquals($subscribers->count(), 2);
        $this->assertEquals($subscribers[0]['user']->id, $userA->id);
        $this->assertEquals($subscribers[1]['user']->id, $userB->id);
        $this->assertEquals(count($subscribers[0]['channels']), 1);
        $this->assertEquals(count($subscribers[1]['channels']), 2);
    }
}
