<?php

namespace Tests\Unit;

use App\Channel;
use App\Event;
use App\Notifications\ReminderPreTriggered;
use App\Notifications\ReminderTriggered;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReminderNotificationTest extends TestCase
{
    /** @test */
    public function a_subscribed_user_receives_a_browser_notification()
    {
        Notification::fake();
        $user = create('App\User');
        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);
        $reminder->subscribe(
            Channel::fromName('browser'),
            Event::fromName('trigger'),
            $user->id
        );
        $code = Artisan::call('reminder:trigger', [
            'time' => Carbon::parse($reminder->trigger)->addMinute(1)
        ]);
        $this->assertEquals(0, $code);
        Notification::assertSentTo(
            $user,
            ReminderTriggered::class,
            function ($notification, $channels) use ($reminder) {
                return $notification->reminder->id === $reminder->id;
            }
        );
    }

    /** @test */
    public function a_not_subscribed_user_does_not_get_a_notification()
    {
        Notification::fake();
        $user = create('App\User');
        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);
        $code = Artisan::call('reminder:trigger', [
            'time' => Carbon::parse($reminder->trigger)->addMinute(1)
        ]);
        $this->assertEquals(0, $code);
        Notification::assertNotSentTo(
            $user,
            ReminderTriggered::class
        );
    }

    /** @test */
    public function multiple_users_can_get_a_reminder_notification()
    {
        Notification::fake();
        $userA = create('App\User');
        $userB = create('App\User');
        $reminder = create('App\Reminder');
        $reminder->subscribe(
            Channel::fromName('browser'),
            Event::fromName('trigger'),
            $userA->id
        );
        $reminder->subscribe(
            Channel::fromName('browser'),
            Event::fromName('trigger'),
            $userB->id
        );
        $code = Artisan::call('reminder:trigger', [
            'time' => Carbon::parse($reminder->trigger)->addMinute(1)
        ]);
        $this->assertEquals(0, $code);
        Notification::assertSentTo(
            $userA,
            ReminderTriggered::class
        );
        Notification::assertSentTo(
            $userB,
            ReminderTriggered::class
        );
    }

    /** @test */
    public function a_subscribed_user_gets_a_pre_trigger_notification()
    {
        Notification::fake();
        $user = create('App\User');
        $reminder = create('App\Reminder', [
            'user_id' => $user->id,
            'trigger' => Carbon::now()->addHour(2)
        ]);
        $subscription = $reminder->subscribe(
            Channel::fromName('browser'),
            Event::fromName('pre-trigger'),
            $user->id,
            [
                "unit" => "hour",
                "value" => "1"
            ]
        );
        $code = Artisan::call('reminder:pre-trigger', [
            'time' => Carbon::parse($reminder->trigger)->addHour(-1)->addMinute(1)
        ]);
        $this->assertEquals(0, $code);
        Notification::assertSentTo(
            $user,
            ReminderPreTriggered::class,
            function ($notification, $channels) use ($reminder) {
                return $notification->reminder->id === $reminder->id;
            }
        );
    }
}
