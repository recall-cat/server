<?php

namespace Tests\Unit;

use App\Events\ReminderTriggered;
use App\Privacy;
use App\Reminder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReminderDateTimeTest extends TestCase
{
    /** @test */
    public function creating_a_reminder_normalizes_the_user_datetime()
    {
        $user = create('App\User');
        $format = 'Y-m-d H:i:s';
        $userTimeZone = 'America/Mexico_City';
        $wallDateTime = '2017-12-24 02:00:00';
        $reminder = Reminder::forceCreate([
            'headline' => 'This is a headline',
            'message' => 'This is a message',
            'trigger' => Carbon::createFromFormat($format, $wallDateTime, $userTimeZone)->toIso8601String(),
            'user_id' => $user->id,
            'privacy_id' => Privacy::fromName('private')
        ]);
        $normalizedDateTime = Carbon::createFromFormat($format, $wallDateTime, $userTimeZone)->setTimezone('UTC')->toIso8601String();
        $this->assertDatabaseHas('reminders', [
            'id' => $reminder->id,
            'headline' => 'This is a headline',
            'message' => 'This is a message',
            'trigger' => $normalizedDateTime,
            'user_id' => $user->id,
            'privacy_id' => Privacy::fromName('private')
        ]);
    }
}
