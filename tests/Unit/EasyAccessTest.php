<?php

namespace Tests\Unit;

use App\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EasyAccessTest extends TestCase
{
    /** @test */
    public function it_lists_all_the_names_correctly()
    {
        $this->assertEquals(
            [
                "trigger",
                "pre-trigger"
            ],
            Event::listNames()
        );
    }
}
