<?php

namespace Tests\Feature;

use App\Events\UserLogged;
use App\Mail\LoginUser;
use App\UserToken;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginUserTest extends TestCase
{
    /** @test */
    public function a_visitor_can_request_login_for_a_claimed_user()
    {
        $this->withoutExceptionHandling();
        \Mail::fake();
        $user = factory(\App\User::class)->states('claimed')->create();
        $request = $this->postJson('/api/login', [
            "email" => $user->email
        ]);
        $request->assertStatus(201);
        \Mail::assertSent(LoginUser::class, function ($mail) use ($user) {
            return $mail->token->user->id === $user->id;
        });
    }

    /** @test */
    public function a_visitor_cannot_request_a_login_while_a_token_is_still_valid()
    {
        $this->withExceptionHandling();
        \Mail::fake();
        $user = factory(\App\User::class)->states('claimed')->create();
        $this->postJson('/api/login', [
            "email" => $user->email
        ])->assertStatus(201);
        $request = $this->postJson('/api/login', [
            "email" => $user->email
        ]);
        $request->assertStatus(403);
    }

    /** @test */
    public function submitting_the_right_code_prevents_its_usage_again()
    {
        $this->passportInstall();
        $user = create('App\User');
        $token = UserToken::generateToken("login", "test@example.com", $user->id);
        $request = $this->postJson("/api/login/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(200);
        $request = $this->postJson("/api/login/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(422);
    }

    /** @test */
    public function submitting_the_right_token_with_email_logins_the_user()
    {
        $this->passportInstall();
        \Mail::fake();
        \Event::fake();
        $user = factory(\App\User::class)->states('claimed')->create();
        $token = UserToken::generateToken("login", $user->email, $user->id);
        $request = $this->postJson("/api/login/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => $user->email
        ]);
        $request->assertStatus(200);
        \Event::assertDispatched(UserLogged::class);
    }
}
