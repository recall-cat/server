<?php

namespace Tests\Feature;

use App\Channel;
use Illuminate\Support\Facades\Event;
use App\Events\ReminderFollowersChanged;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FollowReminderTest extends TestCase
{
    /** @test */
    public function a_non_private_reminder_can_be_followed_by_other_users()
    {
        Event::fake();
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/follow");
        $request->assertStatus(200);
        $request->assertJson([
            'following' => true
        ]);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/follow");
        $request->assertStatus(200);
        $request->assertJson([
            'following' => false
        ]);
        Event::assertDispatched(ReminderFollowersChanged::class, function ($e) use ($reminder) {
            return $e->reminder->id === $reminder->id;
        });
        Event::assertDispatched(ReminderFollowersChanged::class, 2);
    }

    /** @test */
    public function unfollowing_a_reminder_removes_the_subscriptions()
    {
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/follow");
        $request->assertStatus(200);
        $request->assertJson([
            'following' => true
        ]);

        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true
                ]
            ]
        ]);
        $request->assertStatus(200);

        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $user->id,
            'channel_id' => Channel::fromName('browser'),
            'reminder_id' => $reminder->id
        ]);

        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/follow");
        $request->assertStatus(200);
        $request->assertJson([
            'following' => false
        ]);

        $this->assertDatabaseMissing('subscriptions', [
            'user_id' => $user->id,
            'channel_id' => Channel::fromName('browser'),
            'reminder_id' => $reminder->id
        ]);
    }
}
