<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditReminderTest extends TestCase
{
    /** @test */
    public function a_reminder_owner_can_edit_its_reminder()
    {
        $reminder = create('App\Reminder');

        $this->signIn($reminder->user);

        $this->patchJson($reminder->endpoint, [
            "headline" => "edited headline"
        ])
            ->assertStatus(200);

        $this->assertDatabaseHas('reminders', [
            "id" => $reminder->id,
            "headline" => "edited headline"
        ]);
    }
}
