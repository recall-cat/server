<?php

namespace Tests\Feature;

use App\Events\ReminderCreated;
use App\Privacy;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateReminderTest extends TestCase
{
    private $timezone = 'America/Mexico_City';

    private function getDateTime()
    {
        return Carbon::now($this->timezone)->addHours(10)->format('Y-m-d H:00:00');
    }

    /** @test */
    public function a_reminder_can_be_created_without_a_timezone_header()
    {
        $this->signIn();
        $userTimeZone = 'America/Mexico_City';
        $userWallDateTime = Carbon::now($userTimeZone)->addHours(10)->toIso8601String();
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "This is a headline",
            'message' => "I need to do something",
            'datetime' => $userWallDateTime
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'headline' => "This is a headline",
            'message' => "I need to do something"
        ]);
    }

    /** @test */
    public function a_reminder_can_be_created_with_a_timezone_header()
    {
        $this->signIn();
        $userTimeZone = 'America/Mexico_City';
        $userWallDateTime = Carbon::now($userTimeZone)->addHours(10)->format('Y-m-d H:00:00');
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "I need to do something",
            'message' => "This is what I need to do",
            'datetime' => $userWallDateTime
        ], [
            'X-Timezone' => 'America/Mexico_City'
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'message' => "This is what I need to do",
            'headline' => "I need to do something",
        ]);
    }

    /** @test */
    public function a_reminder_cannot_be_created_when_data_is_missing()
    {
        $this->signIn();
        $this->json('POST', '/api/reminders', [
            'headline' => "",
            'message' => "Message is present",
            'datetime' => $this->getDateTime()
        ], [
            'X-Timezone' => $this->timezone
        ])->assertStatus(422);
        $this->json('POST', '/api/reminders', [
            'headline' => "Headline is present",
            'message' => "Message is present",
            'datetime' => ""
        ], [
            'X-Timezone' => $this->timezone
        ])->assertStatus(422);
    }

    /** @test */
    public function a_reminder_cannot_be_created_with_invalid_timezone()
    {
        $this->signIn();
        $this->json('POST', '/api/reminders', [
            'message' => "Message is present",
            'datetime' => $this->getDateTime()
        ], [
            'X-Timezone' => 'xxx'
        ])->assertStatus(422);
    }

    /** @test */
    public function a_reminder_fallback_to_UTC_if_no_timezone_is_provided()
    {
        $this->signIn();
        $this->json('POST', '/api/reminders', [
            'headline' => "Headline is present",
            'message' => "Message is present",
            'datetime' => $this->getDateTime()
        ])->assertStatus(201);
    }

    /** @test */
    public function a_reminder_does_not_require_a_message()
    {
        $this->signIn();
        $this->json('POST', '/api/reminders', [
            'headline' => "Headline is present",
            'datetime' => $this->getDateTime()
        ])->assertStatus(201);
    }

    /** @test */
    public function a_reminder_may_have_a_url ()
    {
        $this->signIn();
        $request = $this->postJson('/api/reminders', [
            'headline' => "Headline is present",
            "url" => "https://google.com",
            'datetime' => $this->getDateTime()
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            "url" => "https://google.com"
        ]);
    }

    /** @test */
    public function a_non_private_reminder_resource_has_a_slug_property()
    {
        $this->signIn();
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Headline is present",
            'datetime' => $this->getDateTime(),
            'privacy' => 'public'
        ]);
        $request->assertStatus(201);
        $request->assertJsonStructure([
            'data' => [
                'slug'
            ]
        ]);
    }
}
