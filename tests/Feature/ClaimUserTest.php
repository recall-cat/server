<?php

namespace Tests\Feature;

use App\ClaimUserToken;
use App\Events\UserClaimed;
use App\Mail\ClaimUser;
use App\UserToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ClaimUserTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->withoutMiddleware([\Illuminate\Routing\Middleware\ThrottleRequests::class]);
    }

    protected function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
        $this->withMiddleware([\Illuminate\Routing\Middleware\ThrottleRequests::class]);
    }

    /** @test */
    public function a_user_can_submit_an_email_to_claim_an_account()
    {
        $this->withoutExceptionHandling();
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(201);
        dump($request->json());
        $request->assertJsonStructure([
            "data" => [
                "email",
                "session_token"
            ]
        ]);
    }

    /** @test */
    public function a_user_cannot_submit_an_email_twice_to_claim_an_account_while_prev_is_still_valid()
    {
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(201);
        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(403);
    }

//    /** @test */
    public function a_user_may_only_request_up_to_1_claims_per_minute()
    {
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(201);
        $this->withMiddleware([\Illuminate\Routing\Middleware\ThrottleRequests::class]);
        $request = $this->postJson('/api/claim', [
            "email" => "different@example.com"
        ]);
        $request->assertStatus(429);
    }

    /** @test */
    public function a_claim_request_fails_if_posted_mail_already_exists()
    {
        $user = create('App\User');
        $user->email = "existing@example.com";
        $user->save();
        $this->signIn();
        $request = $this->postJson('/api/claim', [
            "email" => "existing@example.com"
        ]);
        $request->assertStatus(422);
    }

    /** @test */
    public function a_claim_request_fails_if_user_is_already_claimed()
    {
        $user = create('App\User');
        $user->email = "test@example.com";
        $user->save();
        $this->signIn($user);
        $request = $this->postJson('/api/claim', [
            "email" => "other@example.com"
        ]);
        $request->assertStatus(401);
    }

    /** @test */
    public function submitting_a_claim_emits_an_email_confirmation()
    {
        $user = create('App\User');
        $this->signIn($user);

        Mail::fake();

        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(201);

        Mail::assertSent(ClaimUser::class, function ($mail) use ($user) {
            return $mail->token->user->id === $user->id;
        });
    }

    /** @test */
    public function submitting_a_claim_creates_a_token_model()
    {
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->postJson('/api/claim', [
            "email" => "user@example.com"
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('user_tokens', [
            "user_id" => $user->id
        ]);
    }

    /** @test */
    public function submitting_the_right_token_with_email_sets_the_email_on_the_user()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        $this->withoutExceptionHandling();
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(200);
        $this->assertDatabaseHas('users', [
            "email" => "test@example.com"
        ]);
    }

    /** @test */
    public function submitting_the_right_token_without_email_prevents_set_the_email_on_the_user()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token
        ]);
        $request->assertStatus(422);
        $this->assertDatabaseMissing('users', [
            "email" => "test@example.com"
        ]);
    }

    /** @test */
    public function submitting_the_right_token_with_wrong_email_prevents_set_the_email_on_the_user()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "fake@example.com"
        ]);
        $request->assertStatus(422);
        $this->assertDatabaseMissing('users', [
            "email" => "test@example.com"
        ]);
        $this->assertDatabaseMissing('users', [
            "email" => "fake@example.com"
        ]);
    }

    /** @test */
    public function submitting_the_right_token_with_other_email_prevents_set_the_email_on_the_user()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        UserToken::generateToken("claim", "test2@example.com");
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test2@example.com"
        ]);
        $request->assertStatus(404);
        $this->assertDatabaseMissing('users', [
            "email" => "test@example.com"
        ]);
        $this->assertDatabaseMissing('users', [
            "email" => "test2@example.com"
        ]);
    }

    /** @test */
    public function submitting_the_right_code_sets_the_email_on_the_user()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        $request = $this->postJson("/api/claim/finish", [
            "code" => $token->code,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(200);
        $this->assertDatabaseHas('users', [
            "email" => "test@example.com"
        ]);
    }

    /** @test */
    public function submitting_the_right_code_prevents_its_usage_again()
    {
        $this->passportInstall();
        $user = create('App\User');
        $token = UserToken::generateToken("claim", "test@example.com", $user->id);
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(200);
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(422);
    }

    /** @test */
    public function submitting_a_token_emits_a_notification()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        Event::fake();
        $request = $this->postJson("/api/claim/finish", [
            "confirmation_token" => $token->confirmation_token,
            "email" => "test@example.com"
        ]);
        $request->assertStatus(200);
        Event::assertDispatched(UserClaimed::class);
    }

    /** @test */
    public function a_token_is_only_valid_for_5_minutes()
    {
        $user = create('App\User');
        $this->signIn($user);
        $token = UserToken::generateToken("claim", "test@example.com");
        $valid = $token->isValid();
        $this->assertTrue($valid);
        $valid = $token->isValid(Carbon::now()->addMinute(5));
        $this->assertFalse($valid);
    }
}
