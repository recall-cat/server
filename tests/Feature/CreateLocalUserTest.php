<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateLocalUserTest extends TestCase
{
    /** @test */
    public function a_visitor_can_create_a_local_user()
    {
        $this->passportInstall();
        $response = $this->json('POST', '/api/register', [
            'type' => 'local'
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'token'
        ]);
        $this->assertCount(1, User::all());
    }
}
