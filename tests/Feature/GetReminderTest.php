<?php

namespace Tests\Feature;

use App\Channel;
use App\Privacy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetReminderTest extends TestCase
{
    /** @test */
    public function a_list_of_the_user_reminders_can_be_fetched()
    {
        $this->signIn();
        $request = $this->json('GET', '/api/reminders');
        $request->assertStatus(200);
    }

    /** @test */
    public function a_user_can_fetch_their_reminders()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);
        $request = $this->json('GET', '/api/reminders');
        $request->assertStatus(200);
        $request->assertJson([
            'data' => [
                [
                    'hash' => $reminder->hashed_id,
                    'headline' => $reminder->headline,
                    'message' => $reminder->message,
                ]
            ]
        ]);
    }

    /** @test */
    public function a_user_cannot_see_others_reminders()
    {
        $this->signIn();
        create('App\Reminder');
        $request = $this->json('GET', '/api/reminders');
        $request->assertStatus(200);
        $data = (object)$request->getData();
        $this->assertEmpty($data->data);
    }

    /** @test */
    public function a_non_private_reminder_can_be_fetch()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = create('App\Reminder', [
            'privacy_id' => Privacy::fromName('public')
        ]);
        $request = $this->json('GET', "/api/reminders/$reminder->hashed_id");
        $request->assertStatus(200);
    }

    /**
     * @test
     */
    public function a_user_can_fetch_its_followed_reminders()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = create('App\Reminder');
        $user->follow($reminder->id);
        $request = $this->json('GET', '/api/reminders/followed');
        $request->assertStatus(200);
        $request->assertJson([
            'data' => [
                [
                    'hash' => $reminder->hashed_id,
                    'headline' => $reminder->headline,
                    'message' => $reminder->message,
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function a_reminder_contains_the_number_of_followers()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);
        $request = $this->json('GET', "/api/reminders/$reminder->hashed_id");
        $request->assertStatus(200);
        $request->assertJson([
            "data" => [
                "followers_count" => 0
            ]
        ]);
        create('App\User')->follow($reminder->id);
        create('App\User')->follow($reminder->id);
        create('App\User')->follow($reminder->id);
        $request = $this->json('GET', "/api/reminders/$reminder->hashed_id");
        $request->assertStatus(200);
        $request->assertJson([
            "data" => [
                "followers_count" => 3
            ]
        ]);
    }
}
