<?php

namespace Tests\Feature;

use App\Channel;
use App\Event;
use App\Follow;
use App\Subscription;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotifyReminderTest extends TestCase
{
    private $timezone = 'America/Mexico_City';

    private function getDateTime()
    {
        return Carbon::now($this->timezone)->addHours(10)->format('Y-m-d H:00:00');
    }

    /** @test */
    public function a_reminder_can_be_submitted_with_a_notification_object()
    {
        $user = create('App\User');
        $this->signIn($user);
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Headline is present",
            'datetime' => $this->getDateTime(),
            'notification' => [
                'browser' => true
            ]
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $user->id,
            'reminder_id' => $this->getId($request),
            'event_id' => Event::fromName('trigger'),
            'channel_id' => Channel::fromName('browser'),
        ]);
    }

    /** @test */
    public function the_owner_can_subscribe_to_a_not_followed_reminder_notification()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = factory(\App\Reminder::class)->states('public')->create([
            'user_id' => $user->id
        ]);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true
                ]
            ]
        ]);
        $request->assertStatus(200);
    }

    /** @test */
    public function a_user_cannot_subscribe_to_a_not_followed_reminder_notification()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true
                ]
            ]
        ]);
        $request->assertStatus(403);
    }

    /** @test */
    public function a_user_can_subscribe_to_a_followed_reminder_notification_through_channels()
    {
        $user = create('App\User');
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $this->signIn($user);
        $user->follow($reminder->id);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true
                ]
            ]
        ]);
        $request->assertStatus(200);
        $request->assertJson([
            "trigger" => [
                "browser" => [
                    "value" => true,
                    "data" => null
                ]
            ]
        ]);
        $this->assertDatabaseHas('subscriptions', [
            'reminder_id' => $reminder->id,
            'user_id' => $user->id,
            'channel_id' => Channel::fromName('browser')
        ]);
    }

    /** @test */
    public function a_user_can_unsubscribe_to_a_followed_reminder_notification_through_channels()
    {
        $user = create('App\User');
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $this->signIn($user);
        $user->follow($reminder->id);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true
                ]
            ]
        ]);
        $request->assertStatus(200);
        $request->assertJson([
            "trigger" => [
                "browser" => [
                    "value" => true,
                    "data" => null
                ]
            ]
        ]);
        $this->assertDatabaseHas('subscriptions', [
            'reminder_id' => $reminder->id,
            'user_id' => $user->id,
            'channel_id' => Channel::fromName('browser')
        ]);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => false
                ]
            ]
        ]);
        $request->assertStatus(200);
        $data = $request->json();
        $this->assertEmpty($data);
        $this->assertDatabaseMissing('subscriptions', [
            'reminder_id' => $reminder->id,
            'user_id' => $user->id,
            'channel_id' => Channel::fromName('browser')
        ]);
    }

    /** @test */
    public function a_user_can_fetch_a_reminder_and_see_its_notification_subscription_list()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $user->follow($reminder->id);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => null
                ]
            ]
        ]);
        $request->assertStatus(200);

        $request = $this->json('GET', "/api/reminders/$reminder->hashed_id");
        $request->assertStatus(200);
        $request->assertJson([
            'data' => [
                'notifications' => [
                    'trigger' => [
                        "browser" => [
                            "value" => true,
                            "data" => null
                        ]
                    ]
                ]
            ]
        ]);
    }

    /** @test */
    public function a_user_can_subscribe_to_a_pre_trigger_event_and_change_its_data()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = factory(\App\Reminder::class)->states('public')->create();
        $user->follow($reminder->id);
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'hour',
                        "value" => "2"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(200);
        $subscription = Subscription::first();
        $this->assertEquals($subscription->data->value, 2);
        $this->assertEquals($subscription->data->unit, 'hour');
        $request = $this->json('PATCH', "/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'minute',
                        "value" => "30"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(200);
        $subscription = $subscription->fresh();
        $this->assertEquals($subscription->data->value, 30);
        $this->assertEquals($subscription->data->unit, 'minute');
    }

    /** @test */
    public function the_data_object_in_a_subscription_needs_to_comply_with_validation()
    {
        $user = create('App\User');
        $this->signIn($user);
        $reminder = factory(\App\Reminder::class)->states('public')->create([
            "trigger" => Carbon::now()->addHours(2)->toIso8601String()
        ]);
        $user->follow($reminder->id);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'minute',
                        "value" => "1"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(200);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "cheese" => 'miaw',
                        "blue" => "3"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(422);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'hour',
                        "value" => "1"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(200);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'hour',
                        "value" => "2"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(200);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'hour',
                        "value" => "3"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(422);
        $request = $this->patchJson("/api/reminders/$reminder->hashed_id/notify", [
            "updates" => [
                [
                    "event" => "pre-trigger",
                    "channel" => "browser",
                    "set" => true,
                    "data" => [
                        "unit" => 'blue',
                        "value" => "3"
                    ]
                ]
            ]
        ]);
        $request->assertStatus(422);
    }

    /** @test */
    public function a_visitor_can_request_a_vapid_key()
    {
        $request = $this->json('GET', '/api/vapid');
        $request->assertStatus(200);
        $request->assertJsonStructure([
            'public_key'
        ]);
    }
}
