<?php

namespace Tests\Feature;

use App\Privacy;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Vinkla\Hashids\Facades\Hashids;

class PrivacyReminderTest extends TestCase
{
    /** @test */
    public function a_reminder_is_private_by_default()
    {
        $this->signIn();
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Private reminder",
            'datetime' => Carbon::now()->toDateTimeString()
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'id' => $this->getId($request),
            'privacy_id' => Privacy::fromName('private')
        ]);
    }


    /** @test */
    public function a_reminders_privacy_can_be_set_at_creation()
    {
        $this->signIn();
        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Private reminder",
            'datetime' => Carbon::now()->toDateTimeString(),
            'privacy' => 'public'
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'id' => $this->getId($request),
            'privacy_id' => Privacy::fromName('public')
        ]);

        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Private reminder",
            'datetime' => Carbon::now()->toDateTimeString(),
            'privacy' => 'hidden'
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'id' => $this->getId($request),
            'privacy_id' => Privacy::fromName('hidden')
        ]);

        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Private reminder",
            'datetime' => Carbon::now()->toDateTimeString(),
            'privacy' => 'private'
        ]);
        $request->assertStatus(201);
        $this->assertDatabaseHas('reminders', [
            'id' => $this->getId($request),
            'privacy_id' => Privacy::fromName('private')
        ]);
    }

    /** @test */
    public function a_reminders_privacy_can_be_updated()
    {
        $this->signIn();

        $request = $this->json('POST', '/api/reminders', [
            'headline' => "Private reminder",
            'datetime' => Carbon::now()->toDateTimeString(),
            'privacy' => 'public'
        ]);

        $request->assertStatus(201);

        $id = $this->getId($request);

        $this->assertDatabaseHas('reminders', [
            'id' => $id,
            'privacy_id' => Privacy::fromName('public')
        ]);

        $hash = $this->getHash($request);

        $request = $this->json('PATCH', "/api/reminders/$hash/privacy", [
            'privacy' => 'private'
        ]);

        $request->assertStatus(200);

        $this->assertDatabaseHas('reminders', [
            'id' => $id,
            'privacy_id' => Privacy::fromName('private')
        ]);
    }
}
