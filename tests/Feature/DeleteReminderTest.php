<?php

namespace Tests\Feature;

use App\Channel;
use App\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteReminderTest extends TestCase
{
    /** @test */
    public function a_reminder_can_be_deleted_by_its_owner()
    {
        $user = create('App\User');

        $this->signIn($user);

        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);

        $this->json('DELETE', "/api/reminders/$reminder->hashed_id")
            ->assertStatus(201);

        $this->assertDatabaseMissing('reminders', [
            "id" => $reminder->id
        ]);
    }


    /** @test */
    public function a_reminder_cannot_be_deleted_by_other_users()
    {
        $this->signIn();

        $reminder = create('App\Reminder');

        $this->json('DELETE', "/api/reminders/$reminder->hashed_id")
            ->assertStatus(403);

        $this->assertDatabaseHas('reminders', [
            "id" => $reminder->id
        ]);
    }

    /** @test */
    public function deleting_a_reminder_also_removes_the_subscriptions()
    {
        $user = create('App\User');

        $this->signIn($user);

        $reminder = create('App\Reminder', [
            'user_id' => $user->id
        ]);

        $subscription = $reminder->subscribe(Channel::fromName('browser'), Event::fromName('trigger'));

        $this->json('DELETE', "/api/reminders/$reminder->hashed_id")
            ->assertStatus(201);

        $this->assertDatabaseMissing('reminders', [
            "id" => $reminder->id
        ]);

        $this->assertDatabaseMissing('subscriptions', [
            "id" => $subscription->id
        ]);
    }
}
