<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Vinkla\Hashids\Facades\Hashids;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    use RefreshDatabase;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        Mail::fake();
        $this->artisan('db:seed', ['--class' => 'DatabaseSeeder']);
    }

    protected function signIn($user = null)
    {
        $user = $user ?? create('App\User');
        Passport::actingAs($user);
        return $this;
    }

    protected function passportInstall () {
        $code = Artisan::call('passport:install');
        $this->assertEquals(0, $code);
    }

    protected function getHash ($request) {
        $data = (object)$request->getData();
        return $data->data->hash;
    }

    protected function getId ($request) {
        return Hashids::decode($this->getHash($request))[0];
    }
}
