@component('emails.base-reminder', ["reminder" => $reminder])
    @slot('message')
        Your reminder has been triggered
    @endslot
@endcomponent