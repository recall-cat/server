<mjml>
    <mj-head>
        <mj-font name="Raleway" href="https://fonts.googleapis.com/css?family=Raleway"/>
        <mj-font name="Pacifico" href="https://fonts.googleapis.com/css?family=Pacifico"/>
        <mj-attributes>
            <mj-all font-family="Raleway, Arial, sans-serif"/>
        </mj-attributes>
        <mj-style inline="inline">
            h2 { margin-bottom: 0 !important }
            .shadow { box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0
            rgba(0,0,0,.12) }
        </mj-style>
    </mj-head>
    <mj-body background-color="#FFFFFF" color="#55575d">
        <mj-section background-color="#FFFFFF" align="center" vertical-align="middle" css-class="shadow">
            <mj-column vertical-align="middle" width="150px">
                <mj-image width="150px" src="https://recall.cat/logo.png"/>
            </mj-column>
            <mj-column vertical-align="middle">
                <mj-text font-family="Pacifico" font-size="30px" font-weight="400" font-style="cursive" align="center">
                    <span>Recall Cat</span>
                </mj-text>
            </mj-column>
        </mj-section>
        <mj-spacer height="20px"/>
        @yield('content')
        <mj-spacer height="20px"/>
    </mj-body>
</mjml>