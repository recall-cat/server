@component('emails.base-reminder', ["reminder" => $reminder])
    @slot('message')
        Alert for your reminder has been triggered
    @endslot
@endcomponent