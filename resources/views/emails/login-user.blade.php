@component('mail::message')
# Email confirmation

Please click the button bellow to confirm your log in or use the code directly, pay close attention to your identifier words

<p style="text-align: center; font-size: 24px; background: #eaeaea; padding: 24px; border-radius: 5px"><b>{{$token->identifier}}</b></p>

<p style="text-align: center; font-size: 32px; text-decoration: underline"><b>{{$token->code}}</b></p>

<p style="text-align: center; font-size: 24px; color: gray">Expires in 5 minutes</p>

@component('mail::button', ['url' => url("/login-confirm?confirmation_token=$token->confirmation_token&email=$token->email")])
Confirm Email
@endcomponent

If you can't see/use the button above copy and paste this link in your browser

<a href="{{url("/login-confirm?confirmation_token=$token->confirmation_token&email=$token->email")}}">
    {!! url("/login-confirm?confirmation_token=$token->confirmation_token&email=$token->email") !!}
</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
