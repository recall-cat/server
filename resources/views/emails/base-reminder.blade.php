@extends('emails.base-email')

@section('content')
    <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top">
        <mj-column>
            <mj-text font-size="25px" line-height="60px">
                <h1>{{$message}}</h1>
            </mj-text>
        </mj-column>
    </mj-section>
    <mj-spacer height="20px"/>
    <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top" css-class="shadow">
        <mj-column>
            <mj-text>
                <span>Trigger</span>
                <h2>{{\Carbon\Carbon::parse($reminder->trigger)->format(DateTime::ISO8601)}}</h2>
            </mj-text>
        </mj-column>
    </mj-section>
    <mj-spacer height="20px"/>
    <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top" css-class="shadow">
        <mj-column>
            <mj-text>
                <span>Title</span>
                <h2>{{$reminder->headline}}</h2>
            </mj-text>
        </mj-column>
    </mj-section>
    <mj-spacer height="20px"/>
    @if($reminder->url)
        <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top" css-class="shadow">
            <mj-column>
                <mj-text>
                    <span>Url</span>
                    <h2><a href="{{url($reminder->url)}}">{{url($reminder->url)}}</a></h2>
                </mj-text>
            </mj-column>
        </mj-section>
        <mj-spacer height="20px"/>
    @endif
    @if($reminder->message)
        <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top" css-class="shadow">
            <mj-column>
                <mj-text>
                    <span>Message</span>
                    <h2>{{$reminder->message}}</h2>
                </mj-text>
            </mj-column>
        </mj-section>
        <mj-spacer height="20px"/>
    @endif
    <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top">
        <mj-column>
            <mj-button href="{{url($reminder->public_endpoint)}}" background-color="#009b10" font-size="20px">
                View Reminder
            </mj-button>
        </mj-column>
    </mj-section>
    <mj-spacer height="20px"/>
    <mj-section background-color="#FFFFFF" text-align="center" vertical-align="top" css-class="shadow">
        <mj-column>
            <mj-text>
                <span>If you can't see/use the button above copy and paste this link in your browser</span>
            </mj-text>
            <mj-text>
                <a href="{{url($reminder->public_endpoint)}}">{{url($reminder->public_endpoint)}}</a>
            </mj-text>
        </mj-column>
    </mj-section>
@endsection