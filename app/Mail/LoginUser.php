<?php

namespace App\Mail;

use App\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoginUser extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var UserToken
     */
    public $token;

    /**
     * Create a new message instance.
     *
     * @param UserToken $token
     */
    public function __construct(UserToken $token)
    {
        //
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.login-user')
            ->subject('Please confirm your login request');
    }
}
