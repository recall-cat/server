<?php

namespace App\Mail;

use App\Reminder;
use Illuminate\Bus\Queueable;
use Asahasrabuddhe\LaravelMJML\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyTriggered extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Reminder
     */
    private $reminder;

    /**
     * Create a new message instance.
     *
     * @param Reminder $reminder
     */
    public function __construct(Reminder $reminder)
    {
        //
        $this->reminder = $reminder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->mjml('emails.notify-reminder', [
            "reminder" => $this->reminder
        ]);
    }
}
