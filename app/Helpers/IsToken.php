<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 26-Jan-18
 * Time: 11:38 PM
 */

namespace App\Helpers;


use App\User;
use App\TokenType;
use Auth;
use Carbon\Carbon;

trait IsToken
{
    protected $validForMinutes = 5;

    public function scopeTokenType($scope, string $name)
    {
        return $scope->where("token_type_id", TokenType::fromName($name));
    }

    static function hasValidToken(string $type, string $email)
    {
        $token = self::tokenType($type)->where('email', $email)->first();
        if ($token) {
            return $token->isValid();
        } else {
            return false;
        }
    }

    public function isValid($time = null)
    {
        $valid = $this->created_at->diffInMinutes($time ?? Carbon::now()) < $this->validForMinutes;
        if (!$valid) {
            $this->delete();
        }
        return $valid;
    }

    public function getRouteKeyName()
    {
        return 'token';
    }

    static function generateToken(string $type, string $email, $user_id = null)
    {
        return static::create([
            "user_id" => $user_id ?? Auth::id(),
            "token_type_id" => TokenType::fromName($type),
            "session_token" => str_random(30),
            "confirmation_token" => str_random(30),
            "code" => sprintf("%06d", mt_rand(1, 999999)),
            "identifier" => GenerateWords::identificationWords(),
            "email" => $email
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
