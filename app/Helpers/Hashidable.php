<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 30-Jun-18
 * Time: 7:41 PM
 */

namespace App\Helpers;


trait Hashidable
{
    public function getRouteKey()
    {
        return \Hashids::connection(get_called_class())->encode($this->getKey());
    }
}