<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 29-Dec-17
 * Time: 8:16 PM
 */

namespace App\Helpers;


use Illuminate\Support\Facades\Cache;

trait EasyAccess
{
    public function scopeFromName ($query, string $name) : int {
        return Cache::rememberForever(get_class($this) . "-" . $name, function() use ($query, $name) {
            return $query->where('name', $name)->firstOrFail()->id;
        });
    }

    public function scopeListNames($query)
    {
        return $query->get()->pluck('name')->toArray();
    }
}
