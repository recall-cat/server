<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 04-Feb-18
 * Time: 7:27 PM
 */

namespace App\Helpers;


class GenerateWords
{
    static function getAdjective()
    {
        $f_contents = file(resource_path() . DIRECTORY_SEPARATOR . "words" . DIRECTORY_SEPARATOR . "adjectives.txt");
        $line = $f_contents[rand(0, count($f_contents) - 1)];
        return trim($line);
    }

    static function getAnimal()
    {
        $f_contents = file(resource_path() . DIRECTORY_SEPARATOR . "words" . DIRECTORY_SEPARATOR . "animals.txt");
        $line = $f_contents[rand(0, count($f_contents) - 1)];
        return trim($line);
    }

    static function identificationWords()
    {
        return strtolower(self::getAdjective() . " " . self::getAdjective() . " " . self::getAnimal());
    }
}
