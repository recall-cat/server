<?php

namespace App;

use App\Helpers\EasyAccess;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use EasyAccess;
}
