<?php

namespace App;

use App\Events\ReminderCreated;
use App\Events\ReminderDeleted;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reminder extends Model
{
    protected $dispatchesEvents = [
        'created' => ReminderCreated::class
    ];

    protected static function boot()
    {
        parent::boot();
        self::deleted(function ($reminder) {
            event(new ReminderDeleted($reminder->toArray()));
        });
    }

    protected $with = [
        'privacy'
    ];

    protected $withCount = [
        "followers"
    ];

    protected $fillable = [
        "headline",
        "url",
        "message"
    ];

    function getPublicEndpointAttribute()
    {
        return "/reminders/$this->hashed_id/$this->slugged_headline";
    }

    public function getEndpointAttribute()
    {
        return "/api/reminders/$this->hashed_id";
    }

    public function getHashedIdAttribute()
    {
        return Hashids::encode($this->id);
    }

    public function getSluggedHeadlineAttribute()
    {
        return str_slug($this->headline);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function privacy()
    {
        return $this->belongsTo(Privacy::class);
    }

    public function subscribe(int $channel_id, int $event_id, int $user_id = null, $data = null)
    {
        $subscribed = $this->subscriptions()->where([
            'user_id' => $user_id ?? Auth::id(),
            'reminder_id' => $this->id,
            'channel_id' => $channel_id,
            'event_id' => $event_id
        ])->first();
        if ($subscribed) {
            if ($data) {
                $subscribed->data = $data;
                $subscribed->save();
            }
            return $subscribed;
        };

        $subscription = new Subscription();
        $subscription->user_id = $user_id ?? Auth::id();
        $subscription->reminder_id = $this->id;
        $subscription->channel_id = $channel_id;
        $subscription->event_id = $event_id;
        $subscription->data = $data;
        $subscription->save();
        return $subscription;
    }

    public function unsubscribe(int $channel_id, int $event_id, int $user_id = null)
    {
        $subscribed = $this->subscriptions()->where([
            'user_id' => $user_id ?? Auth::id(),
            'reminder_id' => $this->id,
            'channel_id' => $channel_id,
            'event_id' => $event_id
        ])->first();

        if (!$subscribed) return true;

        $subscribed->delete();

        return $subscribed;
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function followers()
    {
        return $this->hasMany('App\Follow', 'reminder_id', 'id');
    }

    public function userSubscriptions($user_id)
    {
        $subscriptions = $this->subscriptions()
            ->with('channel', 'event')
            ->where('user_id', $user_id)
            ->get();
        $object = [];
        $subscriptions->each(function ($subscription) use (&$object) {
            if (!array_key_exists($subscription->event->name, $object)) {
                $object[$subscription->event->name] = [];
                $object[$subscription->event->name][$subscription->channel->name] = [
                    "value" => true,
                    "data" => $subscription->data
                ];
            } else {
                if (array_key_exists($subscription->channel->name, $object[$subscription->event->name])) {
                    throw new \Error("Repeating channel inside of event list");
                }
                $object[$subscription->event->name][$subscription->channel->name] = [
                    "value" => true,
                    "data" => $subscription->data
                ];
            }
        });
        return $object;
    }

    public function subscribers($event_id)
    {
        $subscriptions = $this->subscriptions()->with([
            'user',
        ])->where('event_id', $event_id)->orderBy('user_id')->get();

        $grouped = collect([]);
        foreach ($subscriptions as $subscription) {
            $currentUserId = $subscription->user->id;
            // search for user
            $existingValue = $grouped->first(function ($value, $key) use ($currentUserId) {
                return $value['user']->id === $currentUserId;
            });
            if ($existingValue) {
                $existingValue['channels']->push($subscription->channel_id);
            } else {
                $grouped->push([
                    'user' => $subscription->user,
                    'channels' => collect([
                        $subscription->channel_id
                    ])
                ]);
            }
        }
        return $grouped;
    }
}
