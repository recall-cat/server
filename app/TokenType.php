<?php

namespace App;

use App\Helpers\EasyAccess;
use Illuminate\Database\Eloquent\Model;

class TokenType extends Model
{
    use EasyAccess;
}
