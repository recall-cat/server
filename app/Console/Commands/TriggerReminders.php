<?php

namespace App\Console\Commands;

use App\Event;
use App\Notifications\ReminderTriggered;
use App\Reminder;
use App\Subscription;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TriggerReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:trigger {time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to trigger all the reminders not yet delivered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = $this->argument('time');
        $reminders = Reminder::where('trigger','<=',$time)->where('dispatched',false)->get();
        $count = $reminders->count();
        $this->info("Triggering $count reminder(s)");
        $reminders->each(function ($reminder) {
            $subscribers = $reminder->subscribers(Event::fromName('trigger'));
            $subscribers->each(function ($subscriber) use ($reminder) {
                try {
                    $reminderId = $reminder->id;
                    $userId = $subscriber['user']->id;
                    $subscriber['user']->notify(new ReminderTriggered($reminder, $subscriber['channels']));
                    $this->info("Notifying user $userId about reminder $reminderId");
                } catch (\Exception $e) {
                    // something went wrong notifying
                    throw $e;
                } finally {
                    // clean database anyway
                    // user might lose the reminder
                    $reminder->subscriptions()->where('user_id', $userId)->where('event_id',Event::fromName('trigger'))->get()->each->delete();
                }
            });
            $reminder->dispatched = true;
            $reminder->save();
        });
    }
}
