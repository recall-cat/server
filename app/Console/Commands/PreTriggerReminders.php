<?php

namespace App\Console\Commands;

use App\Channel;
use App\Event;
use App\Notifications\ReminderPreTriggered;
use App\Subscription;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PreTriggerReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:pre-trigger {time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to trigger all the pre reminders not yet delivered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = $this->argument('time');
        $subscriptions = DB::table('subscriptions')
            ->select('subscriptions.id')
            ->where('event_id', Event::fromName('pre-trigger'))
            ->whereRaw('jsonb_exists(data,\'unit\')')
            ->whereRaw('jsonb_exists(data,\'value\')')
            ->whereRaw('(reminders.trigger - (coalesce(data->>\'value\',\'0\') || \' \' || coalesce(data->>\'unit\',\'minutes\'))::interval) <= ?', [$time])
            ->join('reminders', 'reminders.id', '=', 'subscriptions.reminder_id')
            ->get();
        $subscriptions = Subscription::whereIn('id', $subscriptions->pluck('id'));
        $count = $subscriptions->count();
        $this->info("Pre Triggering $count reminder(s)");
        $subscriptions->each(function ($subscription) {
            $reminderId = $subscription->reminder->id;
            $userId = $subscription->user->id;
            $subscription->user->notify(new ReminderPreTriggered($subscription->reminder, collect([$subscription->channel_id])));
            $this->info("Notifying user $userId about reminder $reminderId");
            $deleted = $subscription->fresh()->delete();
            $deleted = json_encode($deleted);
            $this->info("Deleting: $subscription->id - $deleted");
        });
    }
}
