<?php

namespace App;

use App\Helpers\EasyAccess;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use EasyAccess;
}
