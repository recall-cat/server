<?php

namespace App\Notifications;

use App\Mail\NotifyPreTriggered;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\WebPush\WebPushMessage;

class ReminderPreTriggered extends Notification
{
    use Queueable;
    use HasChannels;
    public $reminder;
    private $channels;

    /**
     * Create a new notification instance.
     *
     * @param $reminder
     * @param $channels
     */
    public function __construct($reminder, $channels)
    {
        //
        $this->reminder = $reminder;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->getChannels($this->channels);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NotifyPreTriggered
     */
    public function toMail($notifiable)
    {
        return (new NotifyPreTriggered($this->reminder))
            ->subject('Reminder pre-notification')
            ->to($notifiable->email);
    }

    public function toWebPush($notifiable)
    {
        return (new WebPushMessage)
            ->title($this->reminder->headline)
            ->body(Carbon::parse($this->reminder->trigger)->diffForHumans())
            ->data([
                'hash' => $this->reminder->hashed_id,
                'slug' => $this->reminder->slugged_headline
            ])
            ->icon('/recall-cat.png');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
