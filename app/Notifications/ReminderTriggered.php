<?php

namespace App\Notifications;

use App\Mail\NotifyTriggered;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\WebPush\WebPushMessage;

class ReminderTriggered extends Notification
{
    use Queueable;
    use HasChannels;

    public $reminder;
    private $channels;

    /**
     * Create a new notification instance.
     *
     * @param $reminder
     * @param $channels
     */
    public function __construct($reminder, $channels)
    {
        //
        $this->reminder = $reminder;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->getChannels($this->channels);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NotifyTriggered
     */
    public function toMail($notifiable)
    {
        return (new NotifyTriggered($this->reminder))
            ->subject('Reminder notification')
            ->to($notifiable->email);
    }

    public function toWebPush($notifiable)
    {
        return (new WebPushMessage)
            ->title($this->reminder->headline)
            ->body($this->reminder->message ?? "Reminder triggered")
            ->requireInteraction()
            ->data([
                'hash' => $this->reminder->hashed_id,
                'slug' => $this->reminder->slugged_headline
            ])
            ->icon('/recall-cat-2.png');
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'reminder_id' => $this->reminder->id,
            'trigger' => $this->reminder->trigger,
            'headline' => $this->reminder->headline,
            'message' => $this->reminder->message,
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
