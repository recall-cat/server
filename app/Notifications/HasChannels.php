<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 05-Jan-18
 * Time: 5:26 PM
 */

namespace App\Notifications;


use App\Channel;
use Illuminate\Notifications\Channels\MailChannel;
use NotificationChannels\WebPush\WebPushChannel;

trait HasChannels
{
    protected function getMappings () {
        return [
            Channel::fromName('browser') => WebPushChannel::class,
            Channel::fromName('mail') => MailChannel::class
        ];
    }

    protected function getChannels($channels)
    {
        $mappings = $this->getMappings();
        $via = [];
        foreach ($channels as $channel) {
            if (array_key_exists($channel, $mappings)) {
                $via[] = $mappings[$channel];
            }
        }
        return $via;
    }
}
