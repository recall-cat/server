<?php

namespace App;

use App\Helpers\EasyAccess;
use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    use EasyAccess;

    public function scopeOptions ($query)
    {
        return $query->get()->pluck('name');
    }
}
