<?php

namespace App\Http\Resources;

use App\Privacy;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class ReminderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hash' => $this->hashed_id,
            'headline' => $this->headline,
            'message' => $this->message,
            'url' => $this->url,
            'trigger' => $this->trigger,
            'created_at' => $this->created_at->timezone('UTC')->toIso8601String(),
            "privacy" => $this->privacy->name,
            "notifications" => $this->userSubscriptions(Auth::id()),
            $this->mergeWhen($this->privacy->id !== Privacy::fromName('private') || $this->user_id === Auth::id(), [
                'slug' => $this->slugged_headline
            ]),
            $this->mergeWhen($this->user_id === Auth::id(), [
                'owner' => true
            ]),
            $this->mergeWhen($this->user_id !== Auth::id(), [
                'following' => Auth::user()->isFollowing($this->id)
            ]),
            "followers_count" => $this->followers_count
        ];
    }
}
