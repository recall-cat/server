<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Event;
use App\Http\Resources\ReminderResource;
use App\Privacy;
use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class ReminderController extends Controller
{
    use TimeHelper;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ReminderResource::collection(Reminder::latest()->where('user_id', Auth::id())->get());
    }

    public function indexFollowed()
    {
        return ReminderResource::collection(Auth::user()->follows);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return ReminderResource
     */
    public function store(Request $request)
    {
        $data = (object)$request->validate([
            "headline" => "required|string",
            "url" => "nullable|string|url",
            "message" => "nullable|string",
            "datetime" => "required|date",
            "notification.browser" => "boolean",
            "notification.email" => "boolean",
            "privacy" => 'nullable|string'
        ]);

        $trigger = $this->getTrigger($request, $data->datetime);


        $reminder = Reminder::forceCreate([
            'user_id' => Auth::id(),
            'trigger' => $trigger,
            'headline' => $data->headline,
            'url' => $data->url ?? null,
            'message' => $data->message ?? null,
            'privacy_id' => Privacy::fromName($data->privacy ?? 'private'),
        ]);

        if (@$data->notification) {
            foreach ($data->notification as $notification => $value) {
                if ($value) {
                    $reminder->subscribe(
                        Channel::fromName($notification),
                        Event::fromName('trigger')
                    );
                }
            }
        }

        return new ReminderResource($reminder);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder $reminder
     * @return ReminderResource
     */
    public function show(Reminder $reminder)
    {
        if ($reminder->privacy->id === Privacy::fromName('private') && Auth::id() !== $reminder->user_id) {
            abort(403);
        }
        return new ReminderResource($reminder);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Reminder $reminder
     * @return ReminderResource
     */
    public function update(Request $request, Reminder $reminder)
    {
        $data = $request->validate([
            "headline" => "required|string",
            "url" => "nullable|string|url",
            "message" => "nullable|string",
        ]);

        $reminder->update($data);

        return new ReminderResource($reminder);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder $reminder
     * @return ReminderResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Reminder $reminder)
    {
        $this->authorize('delete', $reminder);
        $reminder->delete();
        return response('',201);
    }
}
