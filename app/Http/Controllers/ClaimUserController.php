<?php

namespace App\Http\Controllers;

use App\TokenType;
use App\UserToken;
use App\Events\UserClaimed;
use App\Http\Resources\TokenResource;
use App\Mail\ClaimUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class ClaimUserController extends Controller
{
    public function finish()
    {
        $data = request()->validate([
            "email" => [
                "required",
                "email",
                Rule::exists("user_tokens", "email")->where("token_type_id", TokenType::fromName('claim'))
            ],
            "confirmation_token" => [
                "string",
                "required_without:code",
                Rule::exists("user_tokens", "confirmation_token")->where("token_type_id", TokenType::fromName('claim'))
            ],
            "code" => [
                "string",
                "required_without:confirmation_token",
                Rule::exists("user_tokens", "code")->where("token_type_id", TokenType::fromName('claim'))
            ]
        ]);
        if (array_key_exists('confirmation_token', $data)) {
            $claimToken = UserToken::tokenType('claim')->where("confirmation_token", $data['confirmation_token'])->where('email', $data['email'])->firstOrFail();
        } else {
            $claimToken = UserToken::tokenType('claim')->where("code", $data['code'])->where('email', $data['email'])->firstOrFail();
        }
        if ($claimToken->isValid()) {
            $claimToken->user->email = $claimToken->email;
            $claimToken->user->save();
            event(new UserClaimed($claimToken->session_token));
        } else {
            abort(403, 'Token timeout');
        }
        $claimToken->delete();
        return response('', 200);
    }

    public function claim()
    {
        $data = request()->validate([
            "email" => [
                "required",
                "email",
                Rule::unique("users", "email")
            ]
        ]);
        if (Auth::user()->email) {
            abort(401, 'Account already claimed');
        }
        $existingClaimUserToken = UserToken::tokenType('claim')->where('email', $data['email'])->first();
        if ($existingClaimUserToken) {
            if ($existingClaimUserToken->isValid()) {
                abort(403, 'Valid token already created');
            } else {
                $existingClaimUserToken->delete();
            }
        }
        $token = UserToken::generateToken('claim',$data['email']);
        Mail::to($data['email'])->send(new ClaimUser($token));
        return new TokenResource($token);
    }
}
