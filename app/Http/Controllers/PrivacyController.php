<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReminderResource;
use App\Privacy;
use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PrivacyController extends Controller
{
    public function update (Request $request, Reminder $reminder)
    {
        $data = (object)$request->validate([
            'privacy' => [
                'required',
                'string',
                //TODO set with options scope
                Rule::in([
                    'private',
                    'hidden',
                    'public'
                ])
            ]
        ]);

        $reminder->privacy_id = Privacy::fromName($data->privacy);
        $reminder->save();

        return new ReminderResource($reminder);
    }
}
