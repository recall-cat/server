<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 06-Jan-18
 * Time: 3:40 PM
 */

namespace App\Http\Controllers\NotifyValidators;


use App\Reminder;

interface EventValidator
{
    static function validate (?array $data, Reminder $reminder);
}
