<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 06-Jan-18
 * Time: 3:43 PM
 */

namespace App\Http\Controllers\NotifyValidators;


use App\Reminder;
use App\Rules\NumberEqualOrLess;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ValidatePreTrigger implements EventValidator
{

    /**
     * @param $data
     * @param $reminder
     * @return array
     * @throws \Exception
     */
    static function validate(?array $data, Reminder $reminder)
    {

        Validator::make($data, [
            "unit" => [
                "required",
                "string",
                Rule::in([
                    "minute",
                    "hour",
                    "day",
                    "year"
                ])
            ]
        ])->validate();
        $now = Carbon::now();
        $trigger = Carbon::parse($reminder->trigger);
        switch ($data['unit']) {
            case "minute":
                $max = $now->diffInMinutes($trigger);
                break;
            case "hour":
                $max = $now->diffInHours($trigger);
                break;
            case "day":
                $max = $now->diffInDays($trigger);
                break;
            case "year":
                $max = $now->diffInYears($trigger);
                break;
            default:
                throw new \Exception("Unexpected unit type passed base validation");
        }
        Validator::make($data, [
            "value" => [
                "required",
                "integer",
                "min:1",
                new NumberEqualOrLess($max)
            ]
        ])->validate();

        return [
            "unit" => $data['unit'],
            "value" => $data['value'],
        ];
    }
}
