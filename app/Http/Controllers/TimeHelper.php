<?php
/**
 * Created by PhpStorm.
 * User: FeNiX
 * Date: 25-Dec-17
 * Time: 9:53 PM
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;

trait TimeHelper
{
    private function isValidTimezone (string $timezone) {
        return in_array($timezone, timezone_identifiers_list());
    }

    private function getTrigger (Request $request, $datetime)
    {
        $timezone = $request->header('X-Timezone');

        if ($timezone) {
            if (!$this->isValidTimezone($timezone)) {
                abort(422, 'Invalid timezone provided');
            }
            return Carbon::createFromFormat('Y-m-d H:i:s', $datetime, $timezone)->format('Y-m-d H:i:s');
        } else {
            return $datetime;
        }
    }
}
