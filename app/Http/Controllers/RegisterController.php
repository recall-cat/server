<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register ()
    {
        $user = User::create();
        return [
            'token' => $user->createToken('Local')->accessToken,
            'email' => $user->email,
            'id' => $user->hashed_id,
        ];
    }
}
