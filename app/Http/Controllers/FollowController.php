<?php

namespace App\Http\Controllers;

use App\Events\ReminderFollow;
use App\Events\ReminderFollowersChanged;
use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function update(Reminder $reminder)
    {
        $user = Auth::user();
        // cant follow itself
        if ($reminder->user_id === $user->id) {
            abort(403, "Can't follow your own reminder");
        }
        if ($user->isFollowing($reminder->id)) {
            $user->unfollow($reminder->id);
        } else {
            $user->follow($reminder->id);
        }

        event(new ReminderFollow($user));

        event(new ReminderFollowersChanged($reminder));

        return response([
            'following' => !!$user->isFollowing($reminder->id)
        ],200);
    }
}
