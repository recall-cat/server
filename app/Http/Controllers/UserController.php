<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function me()
    {
        return [
            'email' => Auth::user()->email,
            'id' => Auth::user()->hashed_id
        ];
    }
}
