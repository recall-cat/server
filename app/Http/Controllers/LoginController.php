<?php

namespace App\Http\Controllers;

use App\Events\UserLogged;
use App\Http\Resources\TokenResource;
use App\Mail\LoginUser;
use App\TokenType;
use App\User;
use App\UserToken;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LoginController extends Controller
{
    /**
     * @return TokenResource
     * @throws \Throwable
     */
    public function login()
    {
        $data = request()->validate([
            "email" => [
                "required",
                "email"
            ]
        ]);
        $userId = User::where('email', $data['email'])->firstOrFail()->id;
        if (UserToken::hasValidToken("login", $data['email'])) {
            abort(403, "token_still_valid");
        }
        try {
            $token = \DB::transaction(function () use ($userId, $data) {
                $token = UserToken::generateToken("login", $data['email'], $userId);
                \Mail::to($data['email'])->send(new LoginUser($token));
                return $token;
            });
            return new TokenResource($token);
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public function finish()
    {
        $data = request()->validate([
            "email" => [
                "required",
                "email",
                Rule::exists("user_tokens", "email")->where("token_type_id", TokenType::fromName('login'))
            ],
            "confirmation_token" => [
                "string",
                "required_without:code",
                Rule::exists("user_tokens", "confirmation_token")->where("token_type_id", TokenType::fromName('login'))
            ],
            "code" => [
                "string",
                "required_without:confirmation_token",
                Rule::exists("user_tokens", "code")->where("token_type_id", TokenType::fromName('login'))
            ]
        ]);
        if (array_key_exists('confirmation_token', $data)) {
            $claimToken = UserToken::tokenType("login")->where("confirmation_token", $data['confirmation_token'])
                ->where('email', $data['email'])
                ->firstOrFail();
        } else {
            $claimToken = UserToken::tokenType("login")->where("code", $data['code'])
                ->where('email', $data['email'])
                ->firstOrFail();
        }
        if ($claimToken->isValid()) {
            event(new UserLogged([
                "email" => $claimToken->user->email,
                "session_token" => $claimToken->session_token,
                "token" => $claimToken->user->createToken('Login')->accessToken,
            ]));
        } else {
            abort(403, 'Token timeout');
        }
        $claimToken->delete();
        return [
            "login" => "success"
        ];
    }
}
