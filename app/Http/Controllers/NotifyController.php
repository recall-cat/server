<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Event;
use App\Http\Controllers\NotifyValidators\ValidatePreTrigger;
use App\Http\Controllers\NotifyValidators\ValidateTrigger;
use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class NotifyController extends Controller
{
    private function validateBase($request)
    {
        return $request->validate([
            'updates' => 'required|array',
            'updates.*.event' => [
                'required',
                'string',
                Rule::in([
                    'trigger',
                    'pre-trigger'
                ])
            ],
            'updates.*.channel' => [
                'required',
                'string',
                Rule::in([
                    'browser',
                    'mail'
                ])
            ],
            "updates.*.set" => [
                'required',
                'boolean'
            ],
            "updates.*.data" => [
                'nullable'
            ]
        ]);
    }

    protected $dataValidators = [
        "trigger" => ValidateTrigger::class,
        "pre-trigger" => ValidatePreTrigger::class
    ];

    /**
     * @param $event
     * @param $data
     * @param $reminder
     * @return mixed
     * @throws \Exception
     */
    private function validateSubscriptionData(string $event, ?array $data, Reminder $reminder):? array
    {
        if (array_key_exists($event, $this->dataValidators)) {
            return $this->dataValidators[$event]::validate($data, $reminder);
        } else {
            throw new \Exception("Unexpected event passed base validator");
        }
    }

    /**
     * @param Request $request
     * @param Reminder $reminder
     * @return array
     * @throws \Exception
     */
    public function update (Request $request, Reminder $reminder)
    {
        if (Auth::id() !== $reminder->user_id && !Auth::user()->isFollowing($reminder->id)) {
            abort(403, 'Needs to follow the reminder');
        }

        $data = $this->validateBase($request);

        foreach ($data['updates'] as $update) {
            $subscriptionData = $this->validateSubscriptionData($update['event'], $update['data'] ?? null, $reminder);
            if ($update['set']) {
                $reminder->subscribe(Channel::fromName($update['channel']), Event::fromName($update['event']), null, $subscriptionData);
            } else {
                $reminder->unsubscribe(Channel::fromName($update['channel']), Event::fromName($update['event']));
            }
        }

        return $reminder->userSubscriptions(Auth::id());
    }
}
