<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumberEqualOrLess implements Rule
{
    private $limit;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($limit)
    {
        //
        $this->limit = $limit;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (integer)$value <= $this->limit;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Value too high';
    }
}
