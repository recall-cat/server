<?php

namespace App;

use App\Helpers\Hashidable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Vinkla\Hashids\Facades\Hashids;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    use HasPushSubscriptions;
    use Hashidable;

    protected $appends = [
        'hashed_id'
    ];

    public function getHashedIdAttribute()
    {
        return $this->getRouteKey();
    }

    public function isFollowing($reminder_id)
    {
        return $this->follows()->where('reminder_id', $reminder_id)->first();
    }

    public function follow($reminder_id)
    {
        return $this->follows()->attach($reminder_id);
    }

    public function unfollow($reminder_id)
    {
        $this->subscriptions()->where('reminder_id', $reminder_id)->delete();
        return $this->follows()->detach($reminder_id);
    }

    public function follows()
    {
        return $this->belongsToMany('App\Reminder', 'follows', 'user_id', 'reminder_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
