<?php

namespace App;

use App\Helpers\IsToken;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserToken extends Model
{
    use IsToken;

    protected $guarded = [];

    protected $with = ['user'];
}
