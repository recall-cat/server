<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $casts = [
        'data' => 'object',
    ];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function reminder ()
    {
        return $this->belongsTo(Reminder::class);
    }

    public function channel ()
    {
        return $this->belongsTo(Channel::class);
    }

    public function event ()
    {
        return $this->belongsTo(Event::class);
    }
}
