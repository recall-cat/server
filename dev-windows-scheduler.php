<?php

use Illuminate\Support\Carbon;

while (true) {
    if (Carbon::now()->second(0)->toDateTimeString() === $now = Carbon::now()->toDateTimeString()) {
        Artisan::call("test:trigger");
        echo "Triggering $now\n";
    };
    sleep(1);
}
