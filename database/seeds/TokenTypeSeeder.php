<?php

use Illuminate\Database\Seeder;

class TokenTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\TokenType::firstOrCreate([
            'name' => 'login'
        ]);
        \App\TokenType::firstOrCreate([
            'name' => 'claim'
        ]);
    }
}
