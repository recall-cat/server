<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Channel::firstOrCreate([
            'name' => 'browser'
        ]);
        \App\Channel::firstOrCreate([
            'name' => 'mail'
        ]);
    }
}
