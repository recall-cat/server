<?php

use Illuminate\Database\Seeder;

class PrivaciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Privacy::firstOrCreate([
            'name' => 'public'
        ]);
        \App\Privacy::firstOrCreate([
            'name' => 'private'
        ]);
        \App\Privacy::firstOrCreate([
            'name' => 'hidden'
        ]);
    }
}
