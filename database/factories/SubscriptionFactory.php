<?php

use Faker\Generator as Faker;

$factory->define(App\Subscription::class, function (Faker $faker) {
    static $browser;
    static $mail;
    static $trigger;
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'reminder_id' => function () {
            return factory(App\Reminder::class)->create()->id;
        },
        'channel_id' => $faker->randomElement([
            $browser ?? $browser = \App\Channel::fromName('browser'),
            $mail ?? $mail = \App\Channel::fromName('mail'),
        ]),
        'event_id' => $trigger ?? $trigger = \App\Event::fromName('trigger')
    ];
});

$factory->state(App\Subscription::class, 'mail', [
    'channel_id' => \App\Channel::fromName('mail'),
]);

$factory->state(App\Subscription::class, 'browser', [
    'channel_id' => \App\Channel::fromName('browser'),
]);

$factory->state(App\Subscription::class, 'trigger', [
    'event_id' => \App\Event::fromName('trigger'),
]);
