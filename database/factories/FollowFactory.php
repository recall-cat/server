<?php

use Faker\Generator as Faker;

$factory->define(App\Follow::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'reminder_id' => function () {
            return factory(App\Reminder::class)->create()->id;
        },
    ];
});
