<?php

use Faker\Generator as Faker;

$factory->define(App\Reminder::class, function (Faker $faker) {
    static $private;

    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'privacy_id' => $private ?? $private = \App\Privacy::fromName('private'),
        'headline' => $faker->paragraph(1),
        'message' => $faker->paragraph(10),
        'trigger' => \Carbon\Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $faker->dateTimeBetween('1 hour','1 week')->format('Y-m-d H:i:s'),
            date_default_timezone_get()
        )->toIso8601String(),
        'dispatched' => false
    ];
});

$factory->state(App\Reminder::class, 'public', function () {
    static $public;
    return [
        'privacy_id' => $public ?? $public = \App\Privacy::fromName('public'),
    ];
});

$factory->state(App\Reminder::class, 'hidden', function () {
    static $public;
    return [
        'privacy_id' => $public ?? $public = \App\Privacy::fromName('hidden'),
    ];
});
