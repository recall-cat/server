<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeUserTokensTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('claim_user_tokens');
        Schema::create('token_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->integer('token_type_id')->unsigned();
            $table->foreign('token_type_id')->references('id')->on('token_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('claim_user_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string("email")->unique();
            $table->string("session_token")->unique();
            $table->string("confirmation_token")->unique();
            $table->string("identifier")->unique();
            $table->integer("code")->unsigned()->unique();
            $table->timestamps();
        });
        Schema::dropIfExists('token_types');
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->dropColumn('token_type_id');
        });
    }
}
