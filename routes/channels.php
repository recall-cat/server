<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Reminder;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return $user->hashed_id === $id;
});

Broadcast::channel('claim-user.{sessionId}', function ($user, $sessionId) {
    $claimToken = \App\UserToken::where('session_token', $sessionId)->firstOrFail();
    return (int) $user->id === (int) $claimToken->user->id;
});

Broadcast::channel('reminder.{reminder}', function ($user, Reminder $reminder) {
    return $user->id === $reminder->user_id || $reminder->privacy_id !== \App\Privacy::fromName("private");
});
