<?php

use Carbon\Carbon;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('test:trigger', function () {
    $this->call('reminder:trigger', [
        'time' => Carbon::now()
    ]);
});

Artisan::command('test:pre-trigger', function () {
    $this->call('reminder:pre-trigger', [
        'time' => Carbon::now()
    ]);
});
