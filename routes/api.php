<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'LoginController@login');
Route::post('login/finish', 'LoginController@finish');
Route::post('register', 'RegisterController@register');
Route::get('vapid', 'PushSubscriptionController@showVapid');
Route::post('claim/finish', 'ClaimUserController@finish');

Route::middleware('auth:api')->group(function () {
    Route::post('me', 'UserController@me');

    Route::post('claim', 'ClaimUserController@claim')->middleware('throttle:10,1');

    Route::post('subscriptions', 'PushSubscriptionController@update');
    Route::delete('subscriptions', 'PushSubscriptionController@destroy');

    Route::get('reminders/followed', 'ReminderController@indexFollowed');

    Route::apiResource('reminders', 'ReminderController');

    Route::patch('reminders/{reminder}/privacy', 'PrivacyController@update');
    Route::patch('reminders/{reminder}/follow', 'FollowController@update');
    Route::patch('reminders/{reminder}/notify', 'NotifyController@update');
});
